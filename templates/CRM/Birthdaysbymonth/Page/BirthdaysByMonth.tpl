
{*{crmScript ext=org.namelessnetwork.smallgrouptracking url=https://ajax.googleapis.com/ajax/libs/angularjs/1.3.0/angular.js}*}
{crmScript ext=org.namelessnetwork.birthdaysbymonth file=js/angular/angular.min.js}
{crmScript ext=org.namelessnetwork.birthdaysbymonth file=js/BirthdaysByMonth.js}
{crmScript ext=org.namelessnetwork.birthdaysbymonth file=js/angularController.js}
{crmStyle ext=org.namelessnetwork.birthdaysbymonth file=css/BirthdaysByMonth.css}

<div ng-app="birthdaysByMonthApp">
    <div ng-controller="birthdaysCtrl">
        <div class="searchFields">
        <div class="queryField">
            <div class="calloutRow">Select Month</div>
            <select class="filterField" id="thisMonth" ng-model="month">
                <option value="1">January</option>
                <option value="2">February</option>
                <option value="3">March</option>
                <option value="4">April</option>
                <option value="5">May</option>
                <option value="6">June</option>
                <option value="7">July</option>
                <option value="8">August</option>
                <option value="9">September</option>
                <option value="10">October</option>
                <option value="11">November</option>
                <option value="12">December</option>
            </select>
        </div>
        <div id='divSelectGroup' class="queryField"> 
            <div class="calloutRow">Filter by Group (optional)</div>
            <input class="filterField" id="thisGroup" name="field_group" placeholder="{ts}- select -{/ts}"/>
        </div>
        <div id='divSelectDC' class="queryField"> 
            <div class="calloutRow">Filter by Contact Sub Type (optional)</div>
            <input id="thisSubType" class="filterField" name="field_contactSubType" placeholder="{ts}- select -{/ts}"/>
        </div>
        <div class="queryField">
             <div class="calloutRow">Filter Results by String </div>
            <input class="filterField" placeholder="Search" ng-model="searchText">
        </div>
        <button id='bGetBirthdays' class='button' type='button' ng-click='getBirthdays()'>
            Get Birthdays
        </button>
        </div>
        <table id="resultTable" class="selector row-highlight">
            <thead>
                <tr>
                    <th><a ng-click="sort('name')">Name</a></th>
                    <th><a ng-click="sort('age')">Age</a></th>
                    <th><a ng-click="sort('birthday')">Birthday</a></th>
                    <th><a ng-click="sort('address')">Address</a></th>
                </tr>
            </thead>
            <tbody id="resultTableBody">
                <tr id="resultRow" ng-repeat="person in people | filter:searchText | orderBy:predicate:reverse">

                    <td id="name">[[person.name]]</td>
                    <td id="age">[[person.age]]</td>
                    <td id="birthday">[[person.birthday]]</td>
                    <td id="address">[[person.address]]</td>

                </tr>
            </tbody>
        </table>
    </div>
</div>